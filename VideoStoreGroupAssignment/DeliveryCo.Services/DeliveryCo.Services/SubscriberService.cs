﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;
using Common.Model;
using DeliveryCo.Business.Components.Interfaces;
using Microsoft.Practices.ServiceLocation;
using DeliveryCo.Business.Entities;


namespace DeliveryCo.Services
{
    public class SubscriberService: ISubscriberService
    {

        private IDeliveryProvider DeliveryProvider
        {
            get { return ServiceLocator.Current.GetInstance<IDeliveryProvider>(); }
        }


        public void PublishToSubscriber(Common.Model.Message pMessage)
        {
            if (pMessage is DeliveryRequestMessage)
            {
                DeliveryRequestMessage lMessage = pMessage as DeliveryRequestMessage;

                Console.WriteLine("HEY we got your Delivery Request Message ");
                //Console.WriteLine(lMessage.OrderNumber.ToString() + " " + lMessage.DestinationAddress.ToString());

                DeliveryInfo theInfo = new DeliveryInfo();
                theInfo.OrderNumber = lMessage.OrderNumber;
                theInfo.SourceAddress = lMessage.SourceAddress;
                theInfo.DestinationAddress = lMessage.DestinationAddress;
                theInfo.DeliveryNotificationAddress = lMessage.DeliveryNotificationAddress;

                DeliveryProvider.SubmitDeliveryThenMessageBus(theInfo);
            }
        }

    }
}
