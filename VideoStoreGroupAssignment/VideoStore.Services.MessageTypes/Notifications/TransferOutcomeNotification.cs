﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace VideoStore.Services.MessageTypes.Notifications
{
    [DataContract]
    public class TransferOutcomeNotification
    {

        [DataMember]
        public Boolean Success { get; set; }

        [DataMember]
        public Guid OrderNumber { get; set; }
    }
}
