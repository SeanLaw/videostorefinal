﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Common.Model
{
    [DataContract]
    public class EmailMessage: Message
    {
        [DataMember]
        public String ToAddresses { get; set; }
        //public String FromAddresses { get; set; }
        //public String CCAddresses { get; set; }
        //public String BCCAddresses { get; set; }
        //public DateTime Date { get; set; }
        [DataMember]
        public String Message { get; set; }
    }
}
