﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Common.Model
{
    public class DelieveredTellStoreMessage: Message
    {

        [DataMember]
        public String OrderNumber { get; set; }

        [DataMember]
        public Guid DeliveryIdentifier { get; set; }

    }
}
