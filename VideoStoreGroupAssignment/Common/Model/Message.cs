﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace Common.Model
{
    [DataContract]
    //[KnownType]   ?????   can have multiple knowntype,   those subclasses
    [KnownType(typeof(EmailMessage))]
    [KnownType(typeof(DeliveryRequestMessage))]
    [KnownType(typeof(DelieveredTellStoreMessage))]
    public abstract class Message: IVisitable
    {

        [DataMember]
        public String Topic { get; set; }

        public void Accept(IVisitor pVisitor)
        {
            pVisitor.Visit(this);
        }

    }
}
