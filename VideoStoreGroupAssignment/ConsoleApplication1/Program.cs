﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Messaging;
using System.ServiceModel.Description;

namespace MessageBus
{
    class Program
    {

        //VideoStore, EmailServie, DeliveryCo all auto subscribe when they run
        // so make sure it happens at least once, (turn on MessageBus ---> turn on everything else)
        

        private static readonly String sPublishQueuePath = ".\\private$\\PublisherMessageQueueTransacted";

        static void Main(string[] args)
        {

            EnsureMessageQueuesExists();

            using (ServiceHost lPubHost = new ServiceHost(typeof(PublisherService)))
            using (ServiceHost lSubHost = new ServiceHost(typeof(SubscriptionService)))
            {

                AttachMessageInspectorToHost(lPubHost);
                AttachMessageInspectorToHost(lSubHost);
                lPubHost.Open();
                lSubHost.Open();

                Console.WriteLine("MY ASS BUS IS MOVING, Press Q to quit");
                while (Console.ReadKey().Key != ConsoleKey.Q);
            }
        }


        private static void EnsureMessageQueuesExists()
        {
            // Create the transacted MSMQ queue if necessary.
            if (!MessageQueue.Exists(sPublishQueuePath))
            {
                Console.WriteLine("No queue,  Creating one NOW!");
                MessageQueue.Create(sPublishQueuePath, true);
            }
        }



        private static void AttachMessageInspectorToHost(ServiceHost pHost)
        {
            ServiceMessageInspector lInspector = new ServiceMessageInspector();
            foreach (ServiceEndpoint lEndPoint in pHost.Description.Endpoints)
            {
                lEndPoint.Behaviors.Add(lInspector);
            }
        }


    }
}
