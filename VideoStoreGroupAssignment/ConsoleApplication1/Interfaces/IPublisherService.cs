﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Common;////.Model;  <---- need to create that folder later
using Common.Model;

namespace MessageBus.Interfaces
{
//
    [ServiceContract]
    public interface IPublisherService
    {
        [OperationContract(IsOneWay = true)]
        void Publish(Message pMessage); // will change into Message
    }
}
