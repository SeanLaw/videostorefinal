﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;
using Common.Model;
using DeliveryCo.Business.Entities;

namespace DeliveryCo.Business.Components.Transformations
{
    public class DelieveredToDelieveredMessage: IVisitor
    {

        //public Delivered
        public DelieveredTellStoreMessage Result { get; set; }

        public DelieveredToDelieveredMessage(DeliveryInfo pDeliveryInfo)
        {
            Result = new DelieveredTellStoreMessage
            {
                DeliveryIdentifier = pDeliveryInfo.DeliveryIdentifier,
                OrderNumber = pDeliveryInfo.OrderNumber,

                Topic = "TellStoreDelivered"
            };
        }

        public void Visit(IVisitable pVisitable)
        {

        }

    }
}
