﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;
using Common.Model;
using VideoStore.Business.Components.Interfaces;
using Microsoft.Practices.ServiceLocation;

namespace VideoStore.Business.Components
{
    public class SubscriberService: ISubscriberService
    {


        private IDeliveryNotificationProvider DeliveryNotificationProvider
        {
            get { return ServiceLocator.Current.GetInstance<IDeliveryNotificationProvider>(); }
        }

        private IOrderProvider OrderProvider
        {
            get { return ServiceLocator.Current.GetInstance<IOrderProvider>(); }
        }

        public void PublishToSubscriber(Common.Model.Message pMessage)
        {
            if (pMessage is DelieveredTellStoreMessage)
            {
                DelieveredTellStoreMessage lMessage = pMessage as DelieveredTellStoreMessage;
                Console.WriteLine("Hey, the order " + lMessage.OrderNumber.ToString() + " is delivered!");

                Guid theOrderNumber = Guid.Parse(lMessage.OrderNumber.ToString());
                OrderProvider.ReceivedDeliveredAndPublishLast(theOrderNumber);
            }
        }


    }
}
