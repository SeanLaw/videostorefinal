﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.Model;
using VideoStore.Business.Entities;

namespace VideoStore.Business.Components.Transformations
{
    public class DeliveryToDeliveryRequestMessage: IVisitor
    {



        public DeliveryRequestMessage Result { get; set; }

       //

        private String mSourceAddress;
        private String mDestinationAddress;
        private String mOrderNumber;
        private Guid mDeliveryIdentifier;
        private String mDeliveryNotificationAddress;
        private Int32 mStatus;

        public DeliveryToDeliveryRequestMessage(Delivery pDelivery)
        {
            //Guid lDeliveryIdentifier = ExternalServiceFactory.Instance.DeliveryService.SubmitDelivery(new DeliveryInfo()
            //{ 
            //    OrderNumber = lDelivery.Order.OrderNumber.ToString(),  
            //    SourceAddress = lDelivery.SourceAddress,
            //    DestinationAddress = lDelivery.DestinationAddress,
            //    DeliveryNotificationAddress = "net.tcp://localhost:9010/DeliveryNotificationService"
            //});
            mOrderNumber = pDelivery.Order.OrderNumber.ToString();
            mSourceAddress = pDelivery.SourceAddress;
            mDestinationAddress = pDelivery.DestinationAddress;
            mDeliveryNotificationAddress = "net.tcp://localhost:9010/DeliveryNotificationService";

            //lDelivery.ExternalDeliveryIdentifier = lDeliveryIdentifier;
            //pOrder.Delivery = lDelivery;


            Result = new DeliveryRequestMessage
            {
                SourceAddress = pDelivery.SourceAddress,
                OrderNumber = pDelivery.Order.OrderNumber.ToString(),
                DestinationAddress = pDelivery.DestinationAddress,
                
                DeliveryIdentifier = new Guid(),
                DeliveryNotificationAddress = "net.tcp://localhost:9010/DeliveryNotificationService",
                Status = 0,

                Topic = "DeliveryRequest"
                
            };


        }







        public void Visit(IVisitable pVisitable)
        {

            //if (pVisitable is ) ///Order)  
            /////{
            // so i get the order items
            // customer----> message?

            //}
            //if (pVisitable is TradeItem)
            //{
            //    TradeItem lItem = pVisitable as TradeItem;
            //    Result = new PriceChangeMessage() { Item = lItem.Name, Price = lItem.Price, Change = mChange, Topic = lItem.Name };
            //}
        }





    }
}
