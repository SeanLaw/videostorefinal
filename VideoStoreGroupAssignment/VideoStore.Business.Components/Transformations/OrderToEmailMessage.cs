﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.Model;
using VideoStore.Business.Entities;

namespace VideoStore.Business.Components.Transformations
{
    public class OrderToEmailMessage: IVisitor
    {


        //        public DeliveryToDeliveryRequestMessage(Delivery pDelivery)
        //{

        public EmailMessage Result { get; set; }

        public OrderToEmailMessage()
        {
            Result = new EmailMessage
            {
                Topic = "Email"
            };
        }

        public OrderToEmailMessage(Order pOrder)
        {

            Result = new EmailMessage
            {
                ToAddresses = pOrder.Customer.Email,
                Message = "Your order " + pOrder.OrderNumber + " has been placed",
                Topic = "Email"

            };
        }

        public void Visit(IVisitable pVisitable)
        {


            // i believe this is a designemnt preference,
            // i do everything in the constructor

        }
    }
}
