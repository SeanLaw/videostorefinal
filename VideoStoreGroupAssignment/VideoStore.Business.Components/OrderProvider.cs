﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoStore.Business.Components.Interfaces;
using VideoStore.Business.Entities;
using System.Transactions;
using Microsoft.Practices.ServiceLocation;
using DeliveryCo.MessageTypes;
using VideoStore.Business.Components.Transformations;
using VideoStore.Business.Components.PublisherService; // LETS DO IT



namespace VideoStore.Business.Components
{
    public class OrderProvider : IOrderProvider
    {
        public IEmailProvider EmailProvider
        {
            get { return ServiceLocator.Current.GetInstance<IEmailProvider>(); }
        }

        public IUserProvider UserProvider
        {
            get { return ServiceLocator.Current.GetInstance<IUserProvider>(); }
        }

        //Brand New
        public void SubmitOrder(Entities.Order pOrder)
        {
            Console.WriteLine("Just sent a transfer request to the bank - Mr One");

            using (TransactionScope lScope = new TransactionScope())
            { 

                LoadMediaStocks(pOrder);
                MarkAppropriateUnchangedAssociations(pOrder);

                using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
                {
                    pOrder.OrderNumber = Guid.NewGuid();

                    lContainer.Orders.ApplyChanges(pOrder);
                    //lContainer.Orders.AddObject(pOrder);
                    lContainer.SaveChanges();
                    lScope.Complete();
                }
            }

            Console.WriteLine("And the Order number is: " + pOrder.OrderNumber.ToString() + "amount: $" + pOrder.Total.ToString()); //" id: " + pOrder.Id.ToString());
            TransferFundsFromCustomerMessageQueue(UserProvider.ReadUserById(pOrder.Customer.Id).BankAccountNumber, pOrder.Total ?? 0.0, pOrder.OrderNumber);
        }

        public void UpdateStocksAndPublish2Messages(Guid pOrderNumber)
        {
            // this is part 2,  after bank approved
            Order lOrder = getOrderbyOrderNumber(pOrderNumber);
            //Console.WriteLine("$$: " + lOrder.Total.ToString() +"   numbers: " + lOrder.OrderItems.Count.ToString() + "   by: " + lOrder.Customer.ToString() + "  address: " + lOrder.Customer.Address.ToString());


            try 
            {
                UpdateStockCountMyVersion(lOrder);

                Console.WriteLine("The stock count should be deducted");
                Console.WriteLine("One Message to EMail, One Message to Delivery");

                //one for DeliveryCo
                PlaceDevlieryOrderMessageBus(lOrder);

                //one for MessageService
                SendOrderPlacedConfirmatoinMessageBus(lOrder);
            }

            catch
            {

                Console.WriteLine("Meh stock ran out");
                RefundOrderByMessageQueue(UserProvider.ReadUserById(lOrder.Customer.Id).BankAccountNumber, lOrder.Total ?? 0.0, lOrder.OrderNumber);
                Console.WriteLine("Don't worry, we asked a refund for you!");
                //throw;
            }

        }

        //ignored
        public void SubmitOrderOLD(Entities.Order pOrder)
        {

            using (TransactionScope lScope = new TransactionScope())
            {
                LoadMediaStocks(pOrder);
                MarkAppropriateUnchangedAssociations(pOrder);

                using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
                {
                    try
                    {

                        pOrder.OrderNumber = Guid.NewGuid();
                        TransferFundsFromCustomer(UserProvider.ReadUserById(pOrder.Customer.Id).BankAccountNumber, pOrder.Total ?? 0.0);

                        pOrder.UpdateStockLevels();


                        PlaceDeliveryForOrder(pOrder); //  ORIGINAL LINE
                        lContainer.Orders.ApplyChanges(pOrder);
         
                        lContainer.SaveChanges();
                        lScope.Complete();
                        
                    }
                    catch (Exception lException)
                    {
                        SendOrderErrorMessage(pOrder, lException);
                        throw;
                    }
                }
            }
            SendOrderPlacedConfirmation(pOrder);
        }

        private void MarkAppropriateUnchangedAssociations(Order pOrder)
        {
            pOrder.Customer.MarkAsUnchanged();
            pOrder.Customer.LoginCredential.MarkAsUnchanged();
            foreach (OrderItem lOrder in pOrder.OrderItems)
            {
                lOrder.Media.Stocks.MarkAsUnchanged();
                lOrder.Media.MarkAsUnchanged();
            }
        }

        private void LoadMediaStocks(Order pOrder)
        {
            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                foreach (OrderItem lOrder in pOrder.OrderItems)
                {
                    lOrder.Media.Stocks = lContainer.Stocks.Where((pStock) => pStock.Media.Id == lOrder.Media.Id).FirstOrDefault();    
                }
            }
        }

        

        private void SendOrderErrorMessage(Order pOrder, Exception pException)
        {
            EmailProvider.SendMessage(new EmailMessage()
            {
                ToAddress = pOrder.Customer.Email,
                Message = "There was an error in processsing your order " + pOrder.OrderNumber + ": "+ pException.Message +". Please contact Video Store"
            });
        }

        private void SendOrderPlacedConfirmation(Order pOrder)
        {
            EmailProvider.SendMessage(new EmailMessage()
            {
                ToAddress = pOrder.Customer.Email,
                Message = "Your order " + pOrder.OrderNumber + " has been placed"
            });
        }


        private void SendOrderPlacedConfirmatoinMessageBus(Order pOrder)
        {

            OrderToEmailMessage lVisitor = new OrderToEmailMessage(pOrder);
            PublisherServiceClient lClient = new PublisherServiceClient();
            lClient.Publish(lVisitor.Result);
        }


        private void PlaceDeliveryForOrder(Order pOrder)
        {
            Delivery lDelivery = new Delivery() { DeliveryStatus = DeliveryStatus.Submitted, SourceAddress = "Video Store Address", DestinationAddress = pOrder.Customer.Address, Order = pOrder };

            Guid lDeliveryIdentifier = ExternalServiceFactory.Instance.DeliveryService.SubmitDelivery(new DeliveryInfo()
            { 
                OrderNumber = lDelivery.Order.OrderNumber.ToString(),  
                SourceAddress = lDelivery.SourceAddress,
                DestinationAddress = lDelivery.DestinationAddress,
                DeliveryNotificationAddress = "net.tcp://localhost:9010/DeliveryNotificationService"
            });

            lDelivery.ExternalDeliveryIdentifier = lDeliveryIdentifier;
            pOrder.Delivery = lDelivery;
            
        }


        private void PlaceDevlieryOrderMessageBus(Order pOrder)
        {
            Delivery lDelivery = new Delivery() { DeliveryStatus = DeliveryStatus.Submitted, SourceAddress = "Video Store Address", DestinationAddress = pOrder.Customer.Address, Order = pOrder };

            lDelivery.ExternalDeliveryIdentifier = new Guid(); //lolz
            pOrder.Delivery = lDelivery;


            DeliveryToDeliveryRequestMessage lVisitor = new DeliveryToDeliveryRequestMessage(lDelivery);


            //Console.WriteLine("Original order data, OrderNumber:  " + pOrder.OrderNumber.ToString());
            lVisitor.Result.OrderNumber = pOrder.OrderNumber.ToString();

            PublisherServiceClient lClient = new PublisherServiceClient();
            lClient.Publish(lVisitor.Result);
            
        }


        private void TransferFundsFromCustomer(int pCustomerAccountNumber, double pTotal)
        {
            try
            {
                ExternalServiceFactory.Instance.TransferService.Transfer(pTotal, pCustomerAccountNumber, RetrieveVideoStoreAccountNumber());
            }
            catch(Exception e)
            {
                throw new Exception("Error Transferring funds for order.");
            }
        }

        private void TransferFundsFromCustomerMessageQueue(int pCustomerAccountNumber, double pTotal, Guid pOrderNumber)
        {
            // WORKING

            TransferMessageService.TransferMessageServiceClient lClient = new TransferMessageService.TransferMessageServiceClient();
            lClient.TransferMessage(pTotal, pCustomerAccountNumber, RetrieveVideoStoreAccountNumber(), pOrderNumber);

        }


        private int RetrieveVideoStoreAccountNumber()
        {
            return 123;
        }

        private Order getOrderbyOrderNumber(Guid pOrderNumber)
        {
 	        using(VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {

                //working
                //Order theOrder = lContainer.Orders.Include("Customer").Include("OrderItems").Include("Delivery").Where((pOrder) => pOrder.OrderNumber.CompareTo(pOrderNumber) == 0).FirstOrDefault();

                Order theOrder = lContainer.Orders.Include("Customer").Include("OrderItems").Include("Delivery").Include("OrderItems.Media").Where((pOrder) => pOrder.OrderNumber.CompareTo(pOrderNumber) == 0).FirstOrDefault();
                //lOrder.Media

                return theOrder;

            }
        }

        private void UpdateStockCountMyVersion(Order pOrder)
        {
            using (TransactionScope lScope = new TransactionScope())
            {

                LoadMediaStocks(pOrder);

                foreach (OrderItem lOrder in pOrder.OrderItems)
                {
                    lOrder.Media.Stocks.MarkAsUnchanged();
                    lOrder.Media.MarkAsUnchanged();
                }

                using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
                {

                    try
                    {

                        pOrder.UpdateStockLevels();
                        lContainer.Orders.ApplyChanges(pOrder);

                        lContainer.SaveChanges();
                        lScope.Complete();

                    }

                    catch (Exception lException)
                    {
                        //SendOrderErrorMessage(pOrder, lException);
                        Console.WriteLine("For real, out of stock");
                        throw;
                    }

                }
            }
        }


        public void ReceivedDeliveredAndPublishLast(Guid pOrderNumber)
        {
            Order lOrder =  getOrderbyOrderNumber(pOrderNumber);
            OrderToEmailMessage lVisitor = new OrderToEmailMessage(lOrder);
            lVisitor.Result.Message = "Our records show that your order " + pOrderNumber.ToString() + "has been delivered. Thank you for shooping at video store";

            PublisherServiceClient lClient = new PublisherServiceClient();
            lClient.Publish(lVisitor.Result);

            Console.WriteLine("Just told the email about it.");
        }

        // try to implement this, but not really working, because there are some references from the order.
        public void DeleteOrder(Guid pOrderNumber)
        {
            //Order lOrder = getOrderbyOrderNumber(pOrderNumber);

            //using (TransactionScope lScope = new TransactionScope())
            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {

                Order theOrder = lContainer.Orders.Include("Customer").Include("OrderItems").Include("Delivery").Include("OrderItems.Media").Where((pOrder) => pOrder.OrderNumber.CompareTo(pOrderNumber) == 0).FirstOrDefault();
                try
                {

                    theOrder.Customer.MarkAsDeleted();
                    foreach (OrderItem om in theOrder.OrderItems)
                        om.MarkAsDeleted();
                    theOrder.Customer.MarkAsDeleted();

                    lContainer.Orders.DeleteObject(theOrder);
                    
                    lContainer.SaveChanges();
                    //lScope.Complete();

                }
                catch
                {
                    Console.WriteLine("Remove FAILED");
                }

            }

            Console.WriteLine("The order, " + pOrderNumber.ToString() + "should be deleted");

        }

        public void NotifyTransactionFailedToEmail(Guid pOrderNumber)
        {
            Order lOrder = getOrderbyOrderNumber(pOrderNumber);
            OrderToEmailMessage lVisitor = new OrderToEmailMessage(lOrder);
            lVisitor.Result.Message = "There was an error in processing your order " + pOrderNumber.ToString() + ": Error Transferring funds for order..Please contact Video Store";

            PublisherServiceClient lClient = new PublisherServiceClient();
            lClient.Publish(lVisitor.Result);
        }

        private void RefundOrderByMessageQueue(int pCustomerAccountNumber, double pTotal, Guid pOrderNumber)
        {
            TransferMessageService.TransferMessageServiceClient lClient = new TransferMessageService.TransferMessageServiceClient();
            lClient.RefundMessage(pTotal, pCustomerAccountNumber, RetrieveVideoStoreAccountNumber(), pOrderNumber);

        }

    }
}
