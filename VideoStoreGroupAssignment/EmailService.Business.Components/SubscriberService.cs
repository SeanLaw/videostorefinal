﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;
using Common.Model;
//using 

namespace EmailService.Business.Components
{
    public class SubscriberService: ISubscriberService
    {

        public void PublishToSubscriber(Common.Model.Message pMessage)
        {
            if (pMessage is EmailMessage)
            {
                EmailMessage lMessage = pMessage as EmailMessage;
                Console.WriteLine("Sending email to " + lMessage.ToAddresses + ": " + lMessage.Message);
            }
        }
    }
}
