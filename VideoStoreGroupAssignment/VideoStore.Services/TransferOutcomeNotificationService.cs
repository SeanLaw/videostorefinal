﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoStore.Services.Interfaces;
using VideoStore.Services.MessageTypes.Notifications;
using Microsoft.Practices.ServiceLocation;
using VideoStore.Business.Components.Interfaces;

namespace VideoStore.Services
{
    public class TransferOutcomeNotificationService: ITransferOutcomeNotificationService
    {

        private IOrderProvider OrderProvider
        {
            get { return ServiceLocator.Current.GetInstance<IOrderProvider>(); }
        }


        public void NotifyTransferOutcome(TransferOutcomeNotification pOutcome)
        {
            //if(pOutcome.Success = true)
            Console.WriteLine("Hey we got the bank reply for OrderNumber: " + pOutcome.OrderNumber.ToString());

            //if ture

            if (pOutcome.Success == true)
            {

                Console.WriteLine("Success");
                OrderProvider.UpdateStocksAndPublish2Messages(pOutcome.OrderNumber);
                //OrderProvider.SubmitOrder
            }
            else
            {
                Console.WriteLine("Dude you got NO MONEY, now deleting the order and notify email");
                OrderProvider.NotifyTransactionFailedToEmail(pOutcome.OrderNumber);
            }
        }
    }
}
