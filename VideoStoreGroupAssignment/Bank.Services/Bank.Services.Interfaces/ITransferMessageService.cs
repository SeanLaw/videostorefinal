﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Bank.Services.Interfaces
{

    [ServiceContract]
    public interface ITransferMessageService
    {

        [OperationContract(IsOneWay = true)]
        void TransferMessage(double pAmount, int pFromAcctNumber, int pToAcctNumber, Guid pOrderNumber);

        [OperationContract(IsOneWay = true)]
        void RefundMessage(double pAmount, int pFromAcctNumber, int pToAcctNumber, Guid pOrderNumber);
    }
}
