﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.Practices.Unity.ServiceLocatorAdapter;
using Microsoft.Practices.ServiceLocation;
using System.Configuration;

using EmailService.Business.Components.SubscriptionService;
using Common;
using Common.Model;
using EmailService.Business.Components;

namespace EmailService.Process
{
    class Program
    {

        private const String cAddress = "net.msmq://localhost/private/EMailServiceQueueTransacted";
        private const String cMexAddress = "net.tcp://localhost:9114/EMailServiceQueueTransacted/mex";
        //private global::Common.SubscriberServiceHost mHost;


        static void Main(string[] args)
        {
            ResolveDependencies();
            using (ServiceHost lHost = new ServiceHost(typeof(EmailService.Services.EmailService)))  //below is new,  <---- Remove this old thing later
            using (SubscriberServiceHost subHost = new SubscriberServiceHost(typeof(SubscriberService), cAddress, cMexAddress, true, ".\\private$\\EMailServiceQueueTransacted"))  // Nice try
            {
                lHost.Open();
                Console.WriteLine("Email Service Started");

                //trying to (auto) subscribe
                SubscriptionServiceClient lClient = new SubscriptionServiceClient();

                try
                {
                    //lClient.Subscribe("DeliveryRequest", cAddress);
                    //lClient.Unsubscribe("DeliveryRequest", cAddress);
                    lClient.Subscribe("Email", cAddress);
                    Console.WriteLine("Update Subscription Successful!  As MemoryBus is online");
                }
                catch (System.ServiceModel.EndpointNotFoundException e)
                {
                    Console.WriteLine("Just saying, MemeoryBus is offline");
                }


                while (Console.ReadKey().Key != ConsoleKey.Q) ;
            }
        }

        private static void ResolveDependencies()
        {

            UnityContainer lContainer = new UnityContainer();
            UnityConfigurationSection lSection
                    = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            lSection.Containers["containerOne"].Configure(lContainer);
            UnityServiceLocator locator = new UnityServiceLocator(lContainer);
            ServiceLocator.SetLocatorProvider(() => locator);
        }


        /*
        private void HostSubscriberService()
        {
            mHost = new SubscriberServiceHost(typeof(SubscriberService), cAddress, cMexAddress, true, ".\\private$\\TickerQueueTransacted");
        }
         * */
    }
}
