﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Bank.Services.Interfaces;
using Bank.Business.Components.Interfaces;
using System.ServiceModel;
using Microsoft.Practices.ServiceLocation;

namespace Bank.Services
{
    public class TransferMessageService: ITransferMessageService
    {

        private ITransferProvider TransferProvider
        {
            get { return ServiceLocator.Current.GetInstance<ITransferProvider>(); }
        }

        public void TransferMessage(double pAmount, int pFromAcctNumber, int pToAcctNumber, Guid pOrderNumber)
        {
            Console.WriteLine("Hey we received a message for transferring money. ");
            Console.WriteLine("$"+ pAmount.ToString() + " And orderNumber: " +  pOrderNumber.ToString());
            TransferProvider.TransferMessageToNotify(pAmount, pFromAcctNumber, pToAcctNumber, pOrderNumber);
        }

        public void RefundMessage(double pAmount, int pFromAcctNumber, int pToAcctNumber, Guid pOrderNumber)
        {
            Console.WriteLine("Hey we received a message for refunding money. ");
            Console.WriteLine("Refund $:" + pAmount.ToString() + " For orderNumber: " + pOrderNumber.ToString());
            TransferProvider.RefundMessageEnd(pAmount, pFromAcctNumber, pToAcctNumber, pOrderNumber);
        }

    }
}
