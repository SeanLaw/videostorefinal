﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bank.Business.Components.Interfaces;
using Bank.Business.Entities;
using System.Transactions;
using Bank.Services.Interfaces;
using System.Data.SqlClient;

namespace Bank.Business.Components
{
    public class TransferProvider : ITransferProvider
    {


        public void Transfer(double pAmount, int pFromAcctNumber, int pToAcctNumber)
        {
            using (TransactionScope lScope = new TransactionScope())
            using (BankEntityModelContainer lContainer = new BankEntityModelContainer())
            {

                try
                {
                    Account lFromAcct = GetAccountFromNumber(pFromAcctNumber);
                    Account lToAcct = GetAccountFromNumber(pToAcctNumber);
                    lFromAcct.Withdraw(pAmount);
                    lToAcct.Deposit(pAmount);
                    lContainer.Attach(lFromAcct);
                    lContainer.Attach(lToAcct);
                    lContainer.ObjectStateManager.ChangeObjectState(lFromAcct, System.Data.EntityState.Modified);
                    lContainer.ObjectStateManager.ChangeObjectState(lToAcct, System.Data.EntityState.Modified);
                    lContainer.SaveChanges();
                    lScope.Complete();
  
                }          
                catch (Exception lException)
                {
                    

                    if (lException.InnerException is SqlException)
                    {
                        //Console.WriteLine("Seems like an sql Excpetion");
                        throw lException.InnerException;
                    }
                    else
                    {
                        Console.WriteLine("Error occured while transferring money:  " + lException.Message);
                        throw;         
                    }
                }
            }
        }

        public void TransferMessageToNotify(double pAmount, int pFromAcctNumber, int pToAcctNumber, Guid pOrderNumber)
        {
            // sorry for making it so long
            TransferOutcomeNotificationService.TransferOutcomeNotificationServiceClient lClient = new TransferOutcomeNotificationService.TransferOutcomeNotificationServiceClient();

            // suddenly got a genius idea to call the original tranfer function

            // concurrency problem, giving it time/ chance to retry
            int retryCount = 0;
            while(retryCount<1000)
            {
                try
                {
                    Transfer(pAmount, pFromAcctNumber, pToAcctNumber);
                    lClient.NotifyTransferOutcome(new TransferOutcomeNotificationService.TransferOutcomeNotification()
                    {
                        Success = true,
                        OrderNumber = pOrderNumber
                    });
                    Console.WriteLine("Transfer succeed, shooting back a message to Video Store");

                    break;

                }

                catch(SqlException sException)
                {
                    retryCount++;
                    continue; //retry

                }

                catch (Exception lException) // others
                {

                    Console.WriteLine("FAIL A message to Video Store");
                    lClient.NotifyTransferOutcome(new TransferOutcomeNotificationService.TransferOutcomeNotification()
                    {
                    
                        Success = false,
                        OrderNumber = pOrderNumber
                    });
                    break;
                }
            }
        }


        public void RefundMessageEnd(double pAmount, int pFromAcctNumber, int pToAcctNumber, Guid pOrderNumber)
        {
            int retryCount = 0;
            while (retryCount < 1000)
            {
                try
                {
                    Transfer(pAmount, pToAcctNumber, pFromAcctNumber);
                    Console.WriteLine("Refund succeed");
                    break;
                }

                catch (SqlException sException)
                {
                    //Console.WriteLine("Retrying: " + retryCount.ToString());
                    retryCount++;
                    continue; //retry
                }
                catch
                {
                    Console.WriteLine("Refund failed,  somehow");
                    break;
                }
            }

            /*
             *             try
            {
                Transfer(pAmount, pToAcctNumber, pFromAcctNumber);
                Console.WriteLine("Refund succeed");
            }
            catch
            {
                Console.WriteLine("Refund failed,  somehow");
            }
             */
        }

        private Account GetAccountFromNumber(int pToAcctNumber)
        {
            using (BankEntityModelContainer lContainer = new BankEntityModelContainer())
            {
                return lContainer.Accounts.Where((pAcct) => (pAcct.AccountNumber == pToAcctNumber)).FirstOrDefault();
            }
        }
    }
}
