﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ServiceModel;
using VideoStore.Services.MessageTypes.Notifications;

namespace VideoStore.Services.Interfaces
{
    [ServiceContract]
    public interface ITransferOutcomeNotificationService
    {
        [OperationContract(IsOneWay = true)]
        void NotifyTransferOutcome(TransferOutcomeNotification pOutcome);
    }
}
