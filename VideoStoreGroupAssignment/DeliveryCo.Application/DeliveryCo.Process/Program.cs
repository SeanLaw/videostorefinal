﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using DeliveryCo.Services;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.Practices.Unity.ServiceLocatorAdapter;
using Microsoft.Practices.ServiceLocation;
using System.Configuration;
using Common;
using Common.Model;
using DeliveryCo.Business.Components.SubscriptionService;


namespace DeliveryCo.Process
{
    class Program
    {

        private const String cAddress = "net.msmq://localhost/private/DeliveryServiceQueueTransacted";
        private const String cMexAddress = "net.tcp://localhost:9113/DeliveryServiceQueueTransacted/mex";

        //DeliveryServiceQueueTransacted

        static void Main(string[] args)
        {
            ResolveDependencies();
            using (ServiceHost lHost = new ServiceHost(typeof(DeliveryService))) // <--- remove this later
            using (SubscriberServiceHost subHost = new SubscriberServiceHost(typeof(SubscriberService), cAddress, cMexAddress, true, ".\\private$\\DeliveryServiceQueueTransacted")) 
            {
                lHost.Open();  //subHost included .Open() already
                Console.WriteLine("Delivery Service started. Press Q to quit");

                SubscriptionServiceClient lClient = new SubscriptionServiceClient();

                try
                {
                    //lClient.Subscribe("DeliveryRequest", cAddress);
                    //lClient.Unsubscribe("DeliveryRequest", cAddress);
                    lClient.Subscribe("DeliveryRequest", cAddress);
                    Console.WriteLine("Update Subscription Successful!  As MemoryBus is online");
                }
                catch (System.ServiceModel.EndpointNotFoundException e)
                {
                    Console.WriteLine("Just saying, MemeoryBus is offline");
                }

                // remove this part later
                //
                while (Console.ReadKey().Key != ConsoleKey.Q) ;
            }

        }

        private static void ResolveDependencies()
        {

            UnityContainer lContainer = new UnityContainer();
            UnityConfigurationSection lSection
                    = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            lSection.Containers["containerOne"].Configure(lContainer);
            UnityServiceLocator locator = new UnityServiceLocator(lContainer);
            ServiceLocator.SetLocatorProvider(() => locator);
        }
    }
}
