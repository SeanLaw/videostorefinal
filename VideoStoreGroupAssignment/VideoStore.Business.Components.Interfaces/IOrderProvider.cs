﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoStore.Business.Entities;

namespace VideoStore.Business.Components.Interfaces
{
    public interface IOrderProvider
    {
        void SubmitOrder(Order pOrder);

        void UpdateStocksAndPublish2Messages(Guid pOrderNumber);

        void ReceivedDeliveredAndPublishLast(Guid pOrderNumber);

        void DeleteOrder(Guid pOrderNumber);

        void NotifyTransactionFailedToEmail(Guid pOrderNumber);
    }
}
