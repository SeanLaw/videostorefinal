As for the first time setup,  please open the MessageBus first, then open the other application
(Video Store, Bank, DeliveryCo, EmailServer) one by one.


This is because the other applications would attempt to subscript their topic and create the message queues on startup
(you can only subscribe when the messagebus is also online, and this has to happen at least once to make it work).


The coding/ implementation style follows the week6MOM tutorial.
However, as for video store, most calling, processing are implemented within OrderProvider, as the old system used to be.

* There are many more print statment in the console here, which would make it easier to keep track on what is going on.